package com.foodTruckMovement.ftm

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso


class TruckDetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var mTruckName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.truck_details)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.details_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        supportActionBar?.setHomeButtonEnabled(true);
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        mTruckName = intent.getStringExtra("truck_name")
        val mTruckImage = intent.getStringExtra("truck_image")
        val mTruckAddress = intent.getStringExtra("truck_address")
        val mTruckCheckinMessage = intent.getStringExtra("truck_message")
        val mTruckDescription = intent.getStringExtra("truck_description")
        latitude = intent.getDoubleExtra("lat", 0.0)
        longitude = intent.getDoubleExtra("lon", 0.0)

        if (mTruckImage != "null") {
            val truckImageView = findViewById<ImageView>(R.id.truck_image)
            Picasso.get().load(mTruckImage).into(truckImageView)
        }

        findViewById<TextView>(R.id.truck_location).text =  mTruckAddress
        findViewById<TextView>(R.id.truck_name).text =  mTruckName
        findViewById<TextView>(R.id.truck_checkin_message).text =  mTruckCheckinMessage
        findViewById<TextView>(R.id.truck_description).text =  mTruckDescription
        findViewById<TextView>(R.id.truck_description_header).text = "About $mTruckName"

    }

    fun openInMap(view: View) {
        val gmmIntentUri = Uri.parse("geo:0,0?q=$latitude,$longitude($mTruckName)")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.dark_map_style));

        mMap.isMyLocationEnabled = false;
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), 16.0f))

        mMap.apply {
            val position = LatLng(latitude, longitude)
            addMarker(
                MarkerOptions()
                    .icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.location_pin)
                    )
                    .position(position)
            )
        }
    }

}