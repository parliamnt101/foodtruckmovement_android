package com.foodTruckMovement.ftm

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class InfoWindowActivity  : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_window)

        supportActionBar?.setHomeButtonEnabled(true);
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
    }

}