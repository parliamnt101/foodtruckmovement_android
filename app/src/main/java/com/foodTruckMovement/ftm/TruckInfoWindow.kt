package com.foodTruckMovement.ftm

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception
import java.text.SimpleDateFormat

class TruckInfoWindow(val context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(marker: Marker?): View {

        var mInfoView = (context as Activity).layoutInflater.inflate(R.layout.truck_location_info, null)
        var mInfoWindow: InfoWindowData? = marker?.tag as InfoWindowData?

        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
        val formatter = SimpleDateFormat("h:mm a")
        val prettyCheckinTime = formatter.format(parser.parse(mInfoWindow?.mTruckCheckinExpiry.toString()))

        mInfoView.findViewById<TextView>(R.id.truckName).text =  mInfoWindow?.mTruckName
        mInfoView.findViewById<TextView>(R.id.truckCheckinTime).text = prettyCheckinTime

        val truckImageView: ImageView = mInfoView.findViewById(R.id.truck_image)
        if (mInfoWindow?.mTruckImage != "null") {
            val callback = object : Callback {
                override fun onSuccess() {
                    marker!!.showInfoWindow()
                }
                override fun onError(e: Exception?) {
                }
            }

            if (mInfoWindow!!.isVisible) {
                Picasso.get().load(mInfoWindow?.mTruckImage!!).into(truckImageView)
            } else {
                mInfoWindow!!.isVisible = true
                marker!!.tag = mInfoWindow
                Picasso.get().load(mInfoWindow?.mTruckImage!!).into(truckImageView, callback)
            }
        }

        return mInfoView
    }

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }
}