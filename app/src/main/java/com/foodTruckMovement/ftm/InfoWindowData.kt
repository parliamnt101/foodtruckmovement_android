package com.foodTruckMovement.ftm

data class InfoWindowData(
    val mTruckName: String,
    val mTruckImage: String,
    val mTruckCheckinExpiry: String,
    val mTruckDescription: String,
    val mTruckCheckinMessage: String,
    val mTruckAddress: String,
    val mTruckLat: Double,
    val mTruckLon: Double,
    var isVisible: Boolean = false
)