package com.foodTruckMovement.ftm

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import okhttp3.Call
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.io.IOException


class LocationsActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnCameraIdleListener,
    GoogleMap.OnMarkerClickListener,
    GoogleMap.OnMapClickListener,
    GoogleMap.OnInfoWindowClickListener,
    GoogleMap.OnMyLocationClickListener {

    private lateinit var mMap: GoogleMap

    private var locationRequestCode: Int = 1
    private var lastOpened: Marker? = null
    private var isMapReady: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationRequestCode
            )
        }
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            locationRequestCode -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                    if (isMapReady) {
                        setLocationInfo()
                    }

                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun openInfoPage(view: View) {
        val intent = Intent(this, InfoWindowActivity::class.java)
        startActivity(intent)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        isMapReady = true
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));

        val customInfoWindow = TruckInfoWindow(this)
        mMap.setInfoWindowAdapter(customInfoWindow)

        setLocationInfo()
    }

    override fun onCameraIdle() {
        val southwest = mMap.projection.visibleRegion.nearLeft
        val northeast = mMap.projection.visibleRegion.farRight

        mMap.clear()

        val client = OkHttpClient()

        val url = HttpUrl.Builder()
            .scheme(getString(R.string.request_scheme))
            .host(getString(R.string.domain))
            .addPathSegment("api/location/")
            .addQueryParameter("sw_lat", southwest.latitude.toString())
            .addQueryParameter("sw_long", southwest.longitude.toString())
            .addQueryParameter("ne_lat", northeast.latitude.toString())
            .addQueryParameter("ne_long", northeast.longitude.toString())
            .build()

        val get: Request = Request.Builder()
            .url(url)
            .build()

        client.newCall(get).enqueue(object : okhttp3.Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    Toast.makeText(
                        this@LocationsActivity, "Network Error",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                val responseBody: String = response.body!!.string()
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }

                val results = JSONArray(responseBody)

                for (i in 0 until results.length()) {
                    val location = results.getJSONObject(i)
                    val merchant = location.getJSONObject("merchant")
                    val truckCheckinExpiry = location.getString("end")
                    val truckName = merchant.getString("truck_name")
                    val truckDescription = merchant.getString("truck_description")
                    val truckImage = merchant.getString("truck_image")
                    val truckCheckinMessage = location.getString("status")
                    val truckAddress = location.getString("address")
                    val lat = location.getDouble("lat")
                    val long = location.getDouble("lng")

                    val truckInfo = InfoWindowData(
                        truckName, truckImage, truckCheckinExpiry,
                        truckDescription, truckCheckinMessage, truckAddress, lat, long)


                    runOnUiThread {
                        addTruckMarker(lat, long, truckInfo)
                    }
                }
            }
        })
    }

    private fun setLocationInfo() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true;
            mMap.uiSettings.isMyLocationButtonEnabled = true
            mMap.setOnMyLocationButtonClickListener(this)
            mMap.setOnMyLocationClickListener(this)
            mMap.setOnCameraIdleListener(this)
            mMap.setOnMarkerClickListener(this)
            mMap.setOnMapClickListener(this)
            mMap.setOnInfoWindowClickListener(this)

            val mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            mFusedLocationProviderClient.lastLocation
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val mLastKnownLocation = task.result
                        if (mLastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                LatLng(mLastKnownLocation.latitude,
                                    mLastKnownLocation.longitude), 13.0f))
                        }
                    }
                }
        }
    }

    private fun addTruckMarker(lat: Double, lng: Double, info: InfoWindowData) {
        mMap.apply {
            val position = LatLng(lat, lng)
            val marker = addMarker(
                MarkerOptions()
                    .icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.location_pin)
                    )
                    .position(position)
            )
            marker.tag = info
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    override fun onMyLocationClick(location: Location) {
        //Toast.makeText(this, "Current location:\n$location", Toast.LENGTH_LONG).show()
    }

    override fun onInfoWindowClick(marker: Marker?) {
        var mInfoWindow: InfoWindowData? = marker?.tag as InfoWindowData?

        val intent = Intent(this, TruckDetailsActivity::class.java)
        intent.putExtra("truck_name", mInfoWindow?.mTruckName)
        intent.putExtra("truck_message", mInfoWindow?.mTruckCheckinMessage)
        intent.putExtra("truck_description", mInfoWindow?.mTruckDescription)
        intent.putExtra("truck_image", mInfoWindow?.mTruckImage)
        intent.putExtra("truck_address", mInfoWindow?.mTruckAddress)
        intent.putExtra("lat", mInfoWindow?.mTruckLat)
        intent.putExtra("lon", mInfoWindow?.mTruckLon)

        startActivity(intent)
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        if (lastOpened != null) {
            lastOpened!!.hideInfoWindow();
            if (lastOpened!! == marker) {
                lastOpened = null;
                return true;
            }
        }
        marker.showInfoWindow();
        lastOpened = marker;
        return true;
    }

    override fun onMapClick(p0: LatLng?) {
        lastOpened = null;
    }

}
